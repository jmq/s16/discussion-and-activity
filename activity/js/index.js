// 1. In the S16 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 



// 3. Create a variable number that will store the value of the number provided by the user via the prompt.
let n = parseInt(prompt("Input number"))

while(isNaN(n)) {
	n = parseInt(prompt("Input number"))	
}

console.log("Input number: " + n)
// 4. Create a for loop that will be initialized with the number provided by the user, 
// will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
for(let i = n; i > 0; i--) {
	// 5. Create a condition that if the current value is less than or equal to 50, stop the loop.
	if (i <= 50) break

	// 6. Create another condition that if the current value is divisible by 10, 
	// print a message that the number is being skipped and continue to the next iteration of the loop.
	if (i % 10 === 0) {
		console.log("Number skipped.")
		continue
	}

	// 7. Create another condition that if the current value is divisible by 5, print the number.
	if (i % 5 === 0) {
		console.log("Divisible by 5 - " + i)
	}
}


// Stretch Goals:
// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.
let word = "supercalifragilisticexpialidocious"

// 9. Create another variable that will store the consonants from the string.
//const isVowel = x=> !!x.match(/[aeiou]/i)
let consonant = ""

for (let i = 0; i < word.length; i++) {	
	if (word[i] === "a" || word[i] === "e" || word[i] === "i" || word[i] === "o" || word[i] === "u") {				
		continue
	}
	consonant += word[i]
}
console.log("consonant: " + consonant)

let secondVar = ""
let vowel = ""
// 10. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
for (let i = 0; i < word.length; i++) {

	// 11. Create an if statement that will
	// check if the letter of the string is equal to a vowel 
	// and continue to the next iteration of the loop if it is true.
	if (word[i] === "a" || word[i] === "e" || word[i] === "i" || word[i] === "o" || word[i] === "u") {		
		vowel += word[i]				
		continue
	}
	// 12. Create an else statement that will add the letter to the second variable.
	else {
		secondVar += word[i]
	}
}
console.log("vowel: " + vowel)

// 13. Create a git repository named S16.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle.