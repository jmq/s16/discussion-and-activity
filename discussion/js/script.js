var i = 1;                  //  set your counter to 1

// function myLoop() {         //  create a loop function
//   setTimeout(function() {   //  call a 3s setTimeout when the loop is called
//     console.log(i + ' hello');   //  your code here
//     i++;                    //  increment the counter
//     if (i < 10) {           //  if the counter < 10, call the loop function
//      myLoop();             //  ..  again which will trigger another 
//     }                       //  ..  setTimeout()
//   }, 3000)
// }


// myLoop();                   //  start the loop

// let x = 0

// while(x<5) {
//   ++x
//   console.log(x)
// }

// Returns a Promise that resolves after "ms" Milliseconds
const timer = ms => new Promise(res => setTimeout(res, ms))

async function load () { // We need to wrap the loop into an async function for this to work
  for (var i = 0; i < 3; i++) {
    console.log(i);
    await timer(3000); // then the created Promise can be awaited
  }
}

load();